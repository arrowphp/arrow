<?php

declare(strict_types=1);

namespace Arrow\Test;

/**
 * @method fail(string $message)
 */
trait AppLogMixin {


	/**
	 * @var resource|false|null
	 */
	public $testLogStream;
	/**
	 * @var string[]
	 */
	private $expectAppLogEntries = [];
	protected function exceptAppLogMessage(string $message): void {

		$this->expectAppLogEntries[] = $message;
	}

	protected function expectAppLogException(string $class, string $message = null): void {

		$this->expectAppLogEntries[] = "{$class}" . ($message ? " \"{$message}\"" : '');
	}

	private function resetAppLog(): void {

		$this->expectAppLogEntries = [];
	}

	private function cleanupAppLogs(string $path): void {

		foreach ((glob((string)$path . '/logs/*.*') ?: []) as $file) {
			@unlink($file);
		}
	}

	private function checkAppLog(): void {
		$logStream = $this->testLogStream;
		if (!$logStream) {
			return;
		}

		fseek($logStream, 0);
		$logContent = trim(stream_get_contents($logStream) ?: '');
		$hasExpects = (count($this->expectAppLogEntries) > 0);
		$appLogEntries = ($logContent ? explode("\n", $logContent) : []);
		foreach ($appLogEntries as $i => $entry) {
			$entry = json_decode($entry, true);
			foreach ($this->expectAppLogEntries as $k => $expect) {
				if (strpos($entry['message'], $expect) === 0) {
					unset($appLogEntries[$i], $this->expectAppLogEntries[$k]);
					continue;
				}
			}
		}

		if (count($appLogEntries)) {
			$this->fail("Unexpected content written to log file. " . print_r($appLogEntries, true));
		} elseif ($hasExpects) {
			$this->assertTrue(true);
		}

		ftruncate($logStream, 0);
	}
}
