<?php

declare(strict_types=1);

namespace Arrow\Test\Functional;

use Arrow\Event\ApplicationCreateRequestEvent;
use Arrow\Event\ApplicationHandleRequestEvent;
use Arrow\Test\AppTestCase;
use League\Event\BufferedEventDispatcher;

class EventTest extends AppTestCase {

	public function testApplicationCreateRequestEvent(): void {
		$e = null;

		$this->app->get(BufferedEventDispatcher::class)->addListener(
			ApplicationHandleRequestEvent::class,
			function ($event) use (&$e) {
				$e = $event;
				// throw new \Exception('x');
			}
		);

		$this->runApplication($this->createRequest());

		$this->assertNotNull($e);
		$this->assertInstanceOf(ApplicationHandleRequestEvent::class, $e);
	}
}
