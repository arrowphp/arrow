<?php

declare(strict_types=1);

namespace Arrow\Test\Functional;

use Arrow\Test\AppTestCase;
use Arrow;

class ApplicationTest extends AppTestCase {

	public function testReturnsResponse(): void {
		$response = $this->runApplication($this->createRequest());

		$this->assertInstanceOf(\Psr\Http\Message\ResponseInterface::class, $response);
	}

	public function testNoRoute(): void {
		$this->expectAppLogException(\League\Route\Http\Exception\NotFoundException::class);

		// Update route to non-existent path
		// $this->app->get(Router::class)->getNamedRoute('MatchAny');

		$this->runApplication($this->createRequest());
	}
}
