<?php

declare(strict_types=1);

namespace Arrow\Test;

use Arrow\ApplicationBuilder;
use PHPUnit\Framework\TestCase;
use Arrow\Test\AppLogMixin;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;

class AppTestCase extends TestCase {
	use AppLogMixin;

	protected \TestApp\Application $app;

	protected \Closure $appSetup;

	protected function setUp(): void {
		parent::setUp();

		$this->app = new \TestApp\Application(function (ApplicationBuilder $builder) {
			call_user_func([$this, 'appSetup'], $builder);
		});

		// $this->cleanupAppLogs($this->app->get(Config::class)->get('Path.Base'));

		$this->resetAppLog();
	}

	protected function tearDown(): void {
		parent::tearDown();
		$this->checkAppLog();
	}

	protected function runApplication(ServerRequest $request): Response {
		return $this->app->handleWeb($request);
	}

	protected function createRequest(string $path = '/', string $method = null): ServerRequest {
		return new ServerRequest(
			is_string($method) ? $method : 'GET',
			'http://localhost/' . ltrim($path, '/'),
		);
	}

	protected function createJsonRequest(string $path = '/', string $method = null): ServerRequest {
		return new ServerRequest(
			is_string($method) ? $method : 'GET',
			'http://localhost/' . ltrim($path, '/'),
			['Content-Type' => 'application/json'],
		);
	}

	/**
	 * @param array<string, mixed> $vars
	 * @return array<mixed>
	 */
	// public function publicReturnRequestHandler(ServerRequestInterface $request, array $vars): array {
	// 	return [
	// 		'Request' => [
	// 			'URI' => (string)$request->getUri(),
	// 			'Headers' => $request->getHeaders(),
	// 			'Body' => $request->getParsedBody(),
	// 		],
	// 	];
	// }
}
