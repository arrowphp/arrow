<?php

declare(strict_types=1);

namespace Arrow\Test\Unit;

use Arrow\Builder\RouterBuilder;
use Arrow\Router;
use DI\Container;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use TestApp\Application;

/**
 * @group Unit
 * @group Router
 */
class RouterTest extends TestCase {

	private array $routes;

	private Container $container;
	private Router $router;
	private RouterBuilder $routeBuilder;

	public function setUp(): void {
		$this->container = new Container();
		$this->container->set('self', $this);

		$this->router = new Router($this->container);

		$this->routeBuilder = new RouterBuilder('/', $this->routes);
		$this->routeBuilder->get('test', 'test', 'test');
	}

	/**
	 * @param mixed $expected
	 * @dataProvider getValidPathsAndHandlesDataProvider
	 */
	public function testValidPathsAndHandles(string $route, string $path, $expected): void {
		$this->routeBuilder->get($route, 'self', 'handle');

		$request = $this->createRequest('/' . $path);

		$this->router->build($this->routes);
		$result = $this->router->execute($request);

		Assert::assertEquals($expected, $result);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function getValidPathsAndHandlesDataProvider(): array {
		return [
			// Test single level path
			['path', 'path', []],
			// Test multi level path
			['group/path', 'group/path', []],
			// Test path with var
			['group/{id}', 'group/abc', ['id' => 'abc']],
			// Test path with multi var
			['{controller}/{action}', 'group/abc', ['controller' => 'group', 'action' => 'abc']],
		];
	}

	/**
	 * @dataProvider getThrowsExceptionDataProvider
	 */
	public function testThrowsException(string $route, string $handler, string $path, string $exception): void {
		$this->expectExceptionMessage($exception);

		$this->router->group('/', function ($group) use ($route, $handler) {
			$group->get($route, $handler);
		});

		$request = $this->createRequest('/' . $path);
		$this->router->dispatch($request);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function getThrowsExceptionDataProvider(): array {
		return [
			// Test private method on class throws exception
			['test', 'self::handleUnreachable', 'test', 'Could not resolve a callable for this route'],
			// Test undefined method on class throws exception
			['test', 'not-defined', 'test', 'Could not resolve a callable for this route'],
			// Test undefined route throws exception
			['test', 'self::handle', 'not-defined', 'Unable to find handler for path'],
		];
	}

	/**
	 * @param array<string, mixed> $vars
	 * @return array<mixed>
	 */
	public function handle(ServerRequest $request, $vars): array {
		return $vars;
	}

	/**
	 * @param array<string, mixed> $vars
	 * @return array<mixed>
	 */
	private function handleUnreachable(ServerRequest $request, $vars): array {
		return $vars;
	}

	private function createRequest(string $path): ServerRequest {
		return new Request('GET', $path);
	}
}
