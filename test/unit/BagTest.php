<?php

declare(strict_types=1);

namespace Arrow\Test\Unit;

use Arrow\Bag;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

/**
 * @group Unit
 * @group Bag
 */
class BagTest extends TestCase {

	private Bag $bag;
	protected function setUp(): void {
		$this->bag = new Bag();
	}

	/**
	 * @dataProvider bagSetDataProvider
	 * @param mixed $setValue
	 * @param mixed $getValue
	 */
	public function testBagSet(string $setKey, $setValue, string $getKey, $getValue): void {

		$this->bag->set($setKey, $setValue);
		Assert::assertEquals($getValue, $this->bag->get($getKey));
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function bagSetDataProvider(): array {

		return [
			['One', 1, 'One', 1],
			['One.Two', 2, 'One.Two', 2],
			['One.Two.Three', 3, 'One.Two.Three', 3],
			['One.Two.Three', 3, 'One.Two', ['Three' => 3]],
		];
	}
	public function testBagSetReplace(): void {

		$key = 'One';
		$this->bag->set($key, 1);
		Assert::assertEquals(1, $this->bag->get($key));
		$this->bag->set($key, 2);
		Assert::assertEquals(2, $this->bag->get($key));
	}

	public function testBagInit(): void {

		$testConfig = ['TestConfig' => true];
		$this->bag->init($testConfig);
		$config = $this->bag->get();
		Assert::assertNotEmpty($config);
		Assert::assertArrayHasKey('TestConfig', $config);
	}
}
