<?php declare(strict_types=1);

return [
	'Middleware' => [
		\Arrow\Middleware\ModuleLoader::class,
	],
];
