<?php

declare(strict_types=1);

namespace TestApp;

use Arrow\Exception;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Application extends \Arrow\Application {

	public static function createApplicationLogger(string $logDir, bool $isProduction): Logger {
		$stream = fopen('php://memory', 'r+');

		if ($stream === false) {
			throw new Exception('Faild to open testLogStream.');
		}

		$logger = new Logger('Test');

		$formatter = new JsonFormatter();
		$formatter->includeStacktraces();

		$handler = new StreamHandler($stream);
		$handler->setFormatter($formatter);

		$logger->pushHandler($handler);

		return $logger;
	}
}
