<?php

declare(strict_types=1);

namespace Arrow\CLI;

use Arrow\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ServeCommand extends Command {

	private string $passthroughCommand =
		'ARROW_MODE=$mode php ' .
		'-d xdebug.mode=debug ' .
		'-d xdebug.start_with_request=yes ' .
		'-d variables_order=EGPCS ' .
		'-S $host:$port -t $document-root $router';

	protected function configure() {
		$appRoot = Application::$ROOT;
		$arrowRoot = Application::$VENDOR_DIR . '/arrowphp/core';

		$this->setName('serve');
		$this->setAliases(['s']);
		$this->setProcessTitle("Serve");
		$this->setDescription('Serve the application for development or testing. Not recommended for Production.');
		$this->setDefinition([
			new InputArgument(
				'document-root',
				InputArgument::OPTIONAL,
				'Location of your `index.php` file - entry point.',
				"{$appRoot}/http"
			),
			new InputOption(
				'mode',
				null,
				InputOption::VALUE_REQUIRED,
				'Sets `ARROW_MODE`. Only use `production` for testing.',
				'development',
				['development', 'testing', 'production']
			),
			new InputOption('port', 'p', InputOption::VALUE_REQUIRED, 'Set the server port.', '8080'),
			new InputOption(
				'host',
				's',
				InputOption::VALUE_REQUIRED,
				'Set the server host. Use 0.0.0.0 for external access.',
				'localhost'
			),
			new InputOption(
				'router',
				null,
				InputOption::VALUE_REQUIRED,
				'Set the router file to run for the PHP server.',
				"{$arrowRoot}/php-server.php"
			),
		]);
		$this->setHelp(str_replace('%passthroughCommand%', $this->passthroughCommand, <<<'EOF'
The <info>%command.name%</info> command runs your phpunit test suite and outputs an HTML coverage report:

  <info>%command.full_name%</info>

This command that will be executed:

  <info>%passthroughCommand%</info>

This only supports http.

EOF));
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		$data = [
			'$mode' => $input->getOption('mode'),
			'$host' => $input->getOption('host'),
			'$port' => $input->getOption('port'),
			'$document-root' => $input->getArgument('document-root'),
			'$router' => $input->getOption('router'),
		];

		$command = str_replace(
			array_keys($data),
			array_values($data),
			$this->passthroughCommand
		);

		$output->writeln($command);

		if ($data['$mode'] === 'Production') {
			$output->writeln("!! PRODUCTION MODE !!");
		}

		passthru($command, $resultCode);

		return 0; // $resultCode;
	}
}
