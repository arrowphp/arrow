<?php

declare(strict_types=1);

namespace Arrow\Event;

use Symfony\Component\Console\Command\Command;

class ApplicationCliRunBeforeEvent {

	/**
	 * @param Command[] $Commands
	 */
	public function __construct(private array &$Commands) {}

	public function addCommand(Command $command): void {
		$this->Commands[] = $command;
	}
}
