<?php

declare(strict_types=1);

namespace Arrow\Event;

use DI\Container;

class ApplicationBootEvent {

	public function __construct(public readonly Container $container) {
	}
}
