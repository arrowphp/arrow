<?php

declare(strict_types=1);

namespace Arrow;

use Arrow\Interface\Middleware;
use Arrow\Exception\RouteError;
use Arrow\Exception\RouteNotFound;
use Arrow\Object\RouteEndpoint;
use Arrow\Object\RoutePath;
use Arrow\Object\RouteResult;
use Arrow\Utils\Regex;
use GuzzleHttp\Psr7\Request;

class Router {

	/**
	 * @param array<string, string> $methods
	 * @param array<string, RoutePath> $paths
	 * @param array<string, RouteEndpoint> $endpoints
	 * @param array<string, class-string<Middleware>[]> $middlewares
	 */
	public function __construct(
		private array $methods,
		private array $paths,
		private array $endpoints,
		private array $middlewares,
	) {}

	/**
	 * @return array{
	 *   methods: array<string, string>,
	 *   paths: array<string, RoutePath>,
	 *   endpoints: array<string, RouteEndpoint>,
	 *   middlewares: array<string, class-string<Middleware>[]>
	 * }
	 */
	public function __serialize(): array
	{
		return [
			'methods' => $this->methods,
			'paths' => $this->paths,
			'endpoints' => $this->endpoints,
			'middlewares' => $this->middlewares,
		];
	}

	/**
	 * @param array{
	 *   methods: array<string, string>,
	 *   paths: array<string, RoutePath>,
	 *   endpoints: array<string, RouteEndpoint>,
	 *   middlewares: array<string, class-string<Middleware>[]>
	 * } $data
	 */
	public function __unserialize(array $data): void
	{
		[
			'methods' => $this->methods,
			'paths' => $this->paths,
			'endpoints' => $this->endpoints,
			'middlewares' => $this->middlewares,
		] = $data;
	}

	/**
	 * @throws RouteNotFound
	 */
	public function execute(Request $request): RouteResult {
		$method = strtoupper($request->getMethod());

		$route = trim($request->getUri()->getPath(), '/');
		$routeParts = explode('/', $route);

		if (!isset($this->methods[$method])) {
			throw new RouteNotFound($route);
		}

		$id = $this->methods[$method];

		while (isset($this->paths[$id])) {
			$routePath = $this->paths[$id];
			$regex = $routePath->regex;
			$offset = $routePath->offset;

			$subPath = join('/', array_slice($routeParts, $offset));

			$matches = Regex::match($regex, '/' . $subPath . '/', PREG_UNMATCHED_AS_NULL);

			if ($matches === null) {
				throw new RouteNotFound($route, [
					'regex' => $regex,
					'subPath' => $subPath,
				]);
			}

			/** @var array<string, string> $ids */
			$idMatches = array_filter($matches, function ($value, $key) {
				return !is_int($key) && !is_null($value);
			}, ARRAY_FILTER_USE_BOTH);

			/** @var string[] */
			$ids = array_keys($idMatches);
			$id = current($ids);

			if ($id === false) {
				throw new RouteError($route, "Invalid Route Path", [
					'offset' => $offset,
					'regex' => $regex,
					'matches' => $matches,
				]);
			}
		}

		if (!isset($this->endpoints[$id])) {
			throw new RouteNotFound($route);
		}

		$object = $this->endpoints[$id];

		$routeMatches = Regex::match($object->regex, '/' . $route . '/', PREG_UNMATCHED_AS_NULL);

		if ($routeMatches === null) {
			throw new RouteNotFound($route, [
				'regex' => $object->regex,
				'subPath' => $route,
			]);
		}

		$data = array_filter($routeMatches, function ($value, $key) {
			return !is_int($key) && !is_null($value);
		}, ARRAY_FILTER_USE_BOTH);

		return new RouteResult(
			callable: $object->callable,
			data: $data,
			middlewares: $this->getMiddlewaresForRoute('/' . $route),
		);
	}

	/**
	 * @return class-string<Middleware>[]
	 */
	private function getMiddlewaresForRoute(string $route): array {
		$routeMiddlewares = [];

		foreach ($this->middlewares as $subPath => $subPathMiddlewares) {
			if (!str_starts_with($route, $subPath)) {
				continue;
			}

			foreach ($subPathMiddlewares as $subPathMiddleware) {
				if (array_search($subPathMiddleware, $routeMiddlewares, true) === false) {
					array_push($routeMiddlewares, $subPathMiddleware);
				}
			}
		}

		return $routeMiddlewares;
	}

}
