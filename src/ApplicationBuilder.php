<?php

declare(strict_types=1);

namespace Arrow;

use Arrow\Builder\ModulesBuilder;
use Arrow\Builder\ModulesBuilderOptions;
use Arrow\Builder\RouterBuilder;
use Arrow\Builder\RouterBuilderOptions;
use Arrow\Builder\ServicesBuilder;
use Arrow\Builder\ServicesBuilderOptions;
use DI\ContainerBuilder;
use DI\Definition\Source\MutableDefinitionSource;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class ApplicationBuilder {

	/** @var (callable(RouterBuilderOptions $options): void)[] */
	private array $routerHandlers;
	/** @var (callable(ServicesBuilderOptions $options): void)[] */
	private array $serviceHandlers;
	/** @var (callable(ModulesBuilderOptions $options): void)[] */
	private array $moduleHandlers;

	private ApplicationBuilderOptions $options;

	private bool $enableCache;

	public function __construct() {
		$this->routerHandlers = [];
		$this->serviceHandlers = [];
		$this->moduleHandlers = [];
		$this->options = new ApplicationBuilderOptions(
			routerHandlers: $this->routerHandlers,
			serviceHandlers: $this->serviceHandlers,
			moduleHandlers: $this->moduleHandlers,
		);

		$this->enableCache = getenv('ARROW_MODE') === 'Production';

		$this->services(function (ServicesBuilderOptions $options) {
			$options->register(Logger::class, \DI\factory([ApplicationBuilder::class, 'createApplicationLogger'])
				->parameter('logDir', Application::$LOG_DIR)
				->parameter('enabledFileRotating', false));
		});
	}

	public function config(callable $handler): void {
		// TODO
	}

	/**
	 * @param callable(RouterBuilderOptions $options): void $handler
	 */
	public function router(callable $handler): void {
		$this->options->router($handler);
	}

	/**
	 * @param callable(ServicesBuilderOptions $options): void $handler
	 */
	public function services(callable $handler): void {
		$this->options->services($handler);
	}

	/**
	 * @param callable(ModulesBuilderOptions $options): void $handler
	 */
	public function modules(callable $handler): void {
		$this->options->modules($handler);
	}

	public function build(): Application {
		Application::$ROOT = $this->computeProjectRoot();
		Application::$VENDOR_DIR = $this->computeVendorDir();
		// TODO read from config
		Application::$CACHE_DIR = Application::$ROOT . '/.cache';
		Application::$LOG_DIR = Application::$ROOT . '/logs';

		$cacheExists = file_exists(Application::$CACHE_DIR);

		if ($this->enableCache && $cacheExists) {
			return $this->buildFromCache();
		}

		return $this->buildClean($cacheExists);
	}

	private function buildClean(bool $cacheExists): Application {
		$this->buildModules();

		$definitionSource = $this->buildServices();
		$router = $this->buildRouter();

		$containerBuilder = new ContainerBuilder();
		$containerBuilder->addDefinitions($definitionSource);

		if ($this->enableCache && !$cacheExists) {
			$containerBuilder->enableCompilation(Application::$CACHE_DIR);

			// TODO Optimize this caching
			// Create a factory to initialize and hydrate the class
			// - Add `toArray` and `fromArray` methods on the router objects so that the
			//   Container can cache them (because it doesn't handle objects).
			$routerDefinition = \DI\value(serialize($router));
			$routerDefinition->setName('cache.router');

			$definitionSource->addDefinition($routerDefinition);
		}

		$container = $containerBuilder->build();

		return new Application($container, $router);
	}

	private function buildFromCache(): Application {
		$containerBuilder = new ContainerBuilder();
		$containerBuilder->enableCompilation(Application::$CACHE_DIR);
		$container = $containerBuilder->build();

		$router = unserialize($container->get('cache.router'));

		return new Application($container, $router);
	}

	/**
	* @throws Exception
	*/
	protected function computeProjectRoot(): string {
		if (is_string($root = getenv('ARROW_PROJECT_ROOT')) && $root !== "") {
			return $root;
		}
		if (defined('ARROW_PROJECT_ROOT') && is_string($root = constant('ARROW_PROJECT_ROOT')) && $root !== "") {
			return $root;
		}
		if (is_string($documentRoot = $_SERVER['DOCUMENT_ROOT']) && $documentRoot !== "") {
			if (is_string($root = realpath(dirname($documentRoot)))) {
				return $root;
			}
		}

		throw new Exception('Unable to determine "ARROW_PROJECT_ROOT".');
	}

	/**
	 * @throws Exception
	 */
	protected function computeVendorDir(): string {
		if (defined('ARROW_COMPOSER_INSTALL')) {
			$file = realpath(constant('ARROW_COMPOSER_INSTALL'));
			if (!is_string($file) || !file_exists($file)) {
				throw new Exception('Incorrect "ARROW_COMPOSER_INSTALL".');
			}
			return dirname($file);
		}

		foreach ([
			Application::$ROOT . '/../../autoload.php',
			Application::$ROOT . '/../vendor/autoload.php',
			Application::$ROOT . '/vendor/autoload.php'
		] as $file) {
			if (file_exists($file) && is_string($cleanPath = realpath($file))) {
				return dirname($cleanPath);
			}
		}

		throw new Exception('Unable to determine the Vendor directory.');
	}

	public static function createApplicationLogger(string $logDir, bool $enabledFileRotating): Logger {
		$logger = new Logger('App');

		$cliHandler = new StreamHandler('php://stdout');
		$logger->pushHandler($cliHandler);

		$loggerName = strtolower($logger->getName());
		$filename = $logDir . "/{$loggerName}.log";

		if ($enabledFileRotating) {
			$webHandler = new RotatingFileHandler($filename, 0, \Monolog\Level::Info);
		} else {
			$webHandler = new StreamHandler($filename);
		}

		$webHandler->setFormatter(new JsonFormatter(
			batchMode: JsonFormatter::BATCH_MODE_NEWLINES,
			appendNewline: true,
			ignoreEmptyContextAndExtra: true,
			includeStacktraces: true
		));

		$logger->pushHandler($webHandler);

		return $logger;
	}

	private function buildModules(): void {
		$modulesBuilder = new ModulesBuilder();

		while (count($this->moduleHandlers) > 0) {
			foreach ($this->moduleHandlers as $handler) {
				call_user_func($handler, $modulesBuilder->Options);
			}
			$modulesBuilder->build($this->options);
		}
	}

	private function buildServices(): MutableDefinitionSource {
		$servicesBuilder = new ServicesBuilder();

		foreach ($this->serviceHandlers as $handler) {
			call_user_func($handler, $servicesBuilder->Options);
		}

		return $servicesBuilder->build();
	}

	private function buildRouter(): Router {
		$routeBuilder = new RouterBuilder();

		foreach ($this->routerHandlers as $handler) {
			call_user_func($handler, $routeBuilder->Options);
		}

		return $routeBuilder->build();

	}

}
