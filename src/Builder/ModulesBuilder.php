<?php

declare(strict_types=1);

namespace Arrow\Builder;

use Arrow\ApplicationBuilderOptions;
use Arrow\Exception;
use Arrow\Interface\Module;

class ModulesBuilder {

	public readonly ModulesBuilderOptions $Options;
	/** @var (class-string|object)[] */
	private array $modules;

	/** @var array<class-string, true> */
	private array $loadedModules;

	public function __construct() {
		$this->modules = [];
		$this->Options = new ModulesBuilderOptions(
			modules: $this->modules,
		);

		$this->loadedModules = [];

		$this->loadPackageModules();
	}

	/**
	 * @param class-string|object $classOrClassName
	 * @phpstan-assert (class-string<Module>|Module) $classOrClassName
	 */
	public static function IsModule(string|object $classOrClassName): void {
		if (!is_subclass_of($classOrClassName, Module::class)) {
			$className = is_string($classOrClassName) ? $classOrClassName : get_class($classOrClassName);
			throw (new Exception(sprintf("Class '%s' does not implement %s.", $className, Module::class)));
		}
	}

	public function build(ApplicationBuilderOptions $applicationBuilderOptions): void {
		foreach ($this->modules as $module) {
			$this->registerModule($module, $applicationBuilderOptions);
		}
	}

	/**
	 * @param class-string|object $classOrClassName
	 */
	private function registerModule(string|object $classOrClassName, ApplicationBuilderOptions $applicationBuilderOptions): void {
		ModulesBuilder::IsModule($classOrClassName);

		if (is_string($classOrClassName)) {
			/** @var Module|null */
			$class = null;
			/** @var class-string<Module> */
			$className = $classOrClassName;
		} else {
			/** @var Module */
			$class = $classOrClassName;
			/** @var class-string<Module> */
			$className = get_class($class);
		}

		// Prevent registering the same module multiple times.
		// This handles shared dependent modules, and also
		// prevents an infinite loop in the event of error.
		if (isset($this->loadedModules[$className])) {
			// TODO log debug already loaded
			return;
		}

		if ($class === null) {
			$class = new $className();
		}

		$class->register($applicationBuilderOptions);

		$this->loadedModules[$className] = true;
	}

	private function loadPackageModules(): void {
		$packageNames = \Composer\InstalledVersions::getInstalledPackagesByType('arrow-module');
		foreach (array_unique($packageNames) as $packageName) {
			$packageInstallPath = \Composer\InstalledVersions::getInstallPath($packageName);
			$moduleInstallPath = $packageInstallPath  . '/arrow-install.php';

			if (!file_exists($moduleInstallPath)) {
				// TODO log `arrow-module` without `arrow-install.php` file
				continue;
			}

			/** @var Module $module */
			$module = require_once $moduleInstallPath;

			$this->Options->add($module);
		}
	}

}
