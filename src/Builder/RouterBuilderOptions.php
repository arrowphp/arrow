<?php

declare(strict_types=1);

namespace Arrow\Builder;

use Arrow\Interface\Middleware;
use Arrow\Utils\Regex;
use Arrow\Object\RouteCallable;
use Arrow\Object\RouteObject;

class RouterBuilderOptions {

	/**
	 * @param array<string, class-string<Middleware>[]> $middlewares
	 * @param RouteObject[] $routeList
	 */
	public function __construct(
		private string $basePath,
		private array &$middlewares,
		private array &$routeList,
	) {
		// Ensure basePath always begins and ends in a separator.
		$this->basePath = '/' . $this->sanitizeRoute($basePath) . '/';

		// Clean up the root basePath in case it is passed as the root.
		if ($this->basePath === '///') {
			$this->basePath = '/';
		}
	}

	/**
	 * @param class-string<Middleware>[] $middlewares
	 */
	public function add(string $method, string $path, RouteCallable $callable, array $middlewares = []): void {
		$this->routeList[] = new RouteObject(
			method: strtoupper($method),
			route: $this->sanitizeRouteParts($this->basePath, $path),
			callable: $callable,
			middlewares: $middlewares,
		);
	}

	/**
	 * @param class-string<Middleware>[] $middlewares
	 */
	public function get(string $path, RouteCallable $callable, array $middlewares = []): void {
		$this->add('GET', $path, $callable, $middlewares);
	}

	/**
	 * @param class-string<Middleware>[] $middlewares
	 */
	public function post(string $path, RouteCallable $callable, array $middlewares = []): void {
		$this->add('POST', $path, $callable, $middlewares);
	}

	/**
	 * @param callable(RouterBuilderOptions $options): void $handler
	 */
	public function group(string $path, callable $handler): void {
		$options = new RouterBuilderOptions(
			basePath: $this->sanitizeRouteParts($this->basePath, $path),
			middlewares: $this->middlewares,
			routeList: $this->routeList,
		);

		call_user_func($handler, $options);
	}

	/**
	 * @param class-string<Middleware> $middleware
	 */
	public function middleware(string $middleware): void {
		if (!isset($this->middlewares[$this->basePath])) {
			$this->middlewares[$this->basePath] = [];
		}
		if (array_search($middleware, $this->middlewares[$this->basePath], true) === false) {
			array_push($this->middlewares[$this->basePath], $middleware);
		}
	}

	private function sanitizeRoute(string $route): string {
		// Ensure consistency in the way routes are handled
		// in terms of beginning with or ending with a slash.
		$route = trim($route, '/ ');

		// Collapse multiple slashes into single slashes.
		// It's assumed they are a mistake.
		$route = Regex::replace('/\/+/', '/', $route);

		if ($route === '') {
			return '/';
		}

		return $route;
	}

	/**
	 * @param string $routeParts
	 */
	private function sanitizeRouteParts(string ...$routeParts): string {
		// Ensure consistency in the way routes are handled
		// in terms of beginning with or ending with a slash.
		$routeParts = array_map(fn ($part) => $this->sanitizeRoute($part), $routeParts);

		$route = join('/', $routeParts);

		return $this->sanitizeRoute($route);
	}
}
