<?php

declare(strict_types=1);

namespace Arrow\Builder;

use DI\Container;
use DI\Definition\FactoryDefinition;
use DI\Definition\Helper\DefinitionHelper;
use DI\Definition\Source\MutableDefinitionSource;
use DI\Definition\ValueDefinition;

class ServicesBuilderOptions {

	/** @var array<string, true> $cliCommands */
	// @phpstan-ignore property.onlyWritten
	private array $cliCommands;

	private Container $container;

	public function __construct(
		private MutableDefinitionSource $definitionSource,
	)
	{
		$this->container = new Container($this->definitionSource);
	}

	public function containerGet(string $id): mixed {
		return $this->container->get($id);
	}

	// public function subscribeTo(string $event, callable $listener) {
	// 	$this->eventDispatcher->subscribeTo($event, $listener);
	// }

	public function register(string $name, mixed $value): void {
		if ($value instanceof DefinitionHelper) {
			$value = $value->getDefinition($name);
		} elseif ($value instanceof \Closure) {
			$value = new FactoryDefinition($name, $value);
		} else {
			$value = new ValueDefinition($value);
		}

		$value->setName($name);

		$this->definitionSource->addDefinition($value);
	}

	/**
	 * Register a CLI Command.
	 */
	public function registerCliCommand(string $className, DefinitionHelper $definition = null): void {
		if ($definition === null) {
			$definition = \DI\create()->constructor(null);
		}

		$this->cliCommands[$className] = true;
		$this->register($className, $definition);
	}

	public function registerClass(string $className): void {
		$this->registerClassWithKey($className, $className);
	}
	public function registerClassWithKey(string $key, string $className): void {
		$this->register($key, \DI\create($className));
	}

	/**
	 * Register a Class for Arrow Attribute processing.
	 * Also add it into the Container.
	 */
	public function registerAttributes(string $className): void {
		$this->registerAttributesWithKey($className, $className);
	}
	public function registerAttributesWithKey(string $key, string $className): void {
		// TODO setup attribute processing
		$this->registerClassWithKey($key, $className);
	}

	/**
	 * Register a Controller.
	 * Alias for `registerAttributes`;
	 */
	public function registerController(string $className): void {
		$this->registerControllerWithKey($className, $className);
	}
	public function registerControllerWithKey(string $key, string $className): void {
		$this->registerAttributesWithKey($key, $className);
	}

}
