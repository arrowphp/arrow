<?php

declare(strict_types=1);

namespace Arrow\Builder;

class ModulesBuilderOptions {

	/**
	 * @param array<class-sting|object> $modules
	 */
	public function __construct(
		// @phpstan-ignore property.onlyWritten
		private array &$modules
	) {}

	/**
	 * @param class-sting|object $module
	 */
	public function add(string|object $module): void {
		$this->modules[] = $module;
	}
}
