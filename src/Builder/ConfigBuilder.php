<?php

namespace Arrow;

class ConfigBuilder {

	public readonly Config $config;

	public function __construct() {
		$this->config = new Config();
	}

	public function addFile(string $filePath, bool $optional = true): void {
		if (!file_exists($filePath) && !$optional) {
			throw new Exception(sprintf('Required config file "%s" not found.', $filePath));
		}
		if (!str_ends_with($filePath, '.php')) {
			throw new Exception(sprintf('Config file "%s" must be a PHP file.', $filePath));
		}

		$config = $this->safeRequireFile($filePath);

		if (!is_array($config)) {
			throw new Exception(
				sprintf('Config file "%s" must return an array.', $filePath),
				['returned' => $config],
			);
		}

		$this->config->init($config);
	}

	public function addEnvironmentVariables(string $prefix): void {
		// TODO implement
		// - read over all environment variables matching prefix
		// - removing prefix, rest of the name should be a valid config path
		//   - set/replace the value in the config with the environment value
	}

	/** @return array<string, mixed>|mixed */
	private function safeRequireFile(string $path): array {
		if (!file_exists($path)) {
			return [];
		}

		ob_start();
		$return = require($path);
		ob_end_clean();

		return $return;
	}
}
