<?php

declare(strict_types=1);

namespace Arrow;

class ConfigManager {
	private string $selfPath;
	private string $basePath;

	public function __construct() {
		$this->selfPath = ARROW_PATH_SELF;
		$this->basePath = ARROW_PATH_BASE;
	}

	/**
	 * @return array<mixed, mixed>
	 */
	public function loadConfig(): array {
		$packages = $this->locatePackages();
		$config = $this->mergeAllConfigFiles($packages);

		$config['Path'] = [
			'Self' => $this->selfPath,
			'Base' => $this->basePath,
		];

		return $config;
	}

	/**
	 * @return array<string, string>
	 */
	private function locatePackages(): array {
		$composerFile = $this->basePath . '/composer.json';

		if (!file_exists($composerFile)) {
			return [];
		}

		$composer = json_decode(file_get_contents($composerFile) ?: '', true);

		if (empty($composer['require'])) {
			return [];
		}

		$packages = [];
		$vendorDir = $this->basePath . '/vendor';
		foreach ($composer['require'] as $packageName => $version) {
			$packagePath = $vendorDir . '/' . $packageName;

			if (file_exists($packagePath . '/arrow-install.php')) {
				$packages[(string)$packageName] = $packagePath;
			}
		}

		return $packages;
	}

	/**
	 * @param array<mixed, mixed> $packages
	 * @return array<mixed, mixed>
	 */
	private function mergeAllConfigFiles(array $packages): array {
		$config = array_replace_recursive(
			$this->safeRequire($this->selfPath . '/config/config.php', []),
			$this->safeRequire($this->basePath . '/config/config.php', [])
		);

		foreach ((glob($this->selfPath . '/config/config.*.php', GLOB_BRACE) ?: []) as $configPath) {
			$config = array_replace_recursive($config, $this->safeRequire($configPath, []));
		}

		foreach ((glob($this->basePath . '/config/config.*.php', GLOB_BRACE) ?: []) as $configPath) {
			$config = array_replace_recursive($config, $this->safeRequire($configPath, []));
		}

		$configPackages = [];
		foreach ($packages as $packageName => $packagePath) {
			$configPath = "{$packagePath}/config/config.php";

			if (!file_exists($configPath)) {
				continue;
			}

			$configPackages[$packageName] = $this->safeRequire($configPath, []);
			$configPackages[$packageName]['Path'] = [
				'Base' => $packagePath,
				'Self' => $this->selfPath,
			];
		}

		$config = array_replace_recursive(['Package' => $configPackages], $config);

		foreach ($packages as $packageName => $packagePath) {
			if (isset($config['Package'][$packageName]['_Mutator_'])) {
				$mutator = $config['Package'][$packageName]['_Mutator_'];
				$mutator($config);
			}

			if (isset($config['Package'][$packageName]['_Environment_'])) {
				foreach ($config['Package'][$packageName]['_Environment_'] as $env => $path) {
					if (($newValue = getenv($env)) == false) {
						continue;
					}

					$value = &$config['Package'][$packageName];
					foreach (explode('.', $path) as $key) {
						if (array_key_exists($key, $value)) {
							$value = &$value[$key];
						} else {
							continue 2;
						}
					}

					$value = $newValue;
				}
			}

			unset($config['Package'][$packageName]['_Mutator_']);
			unset($config['Package'][$packageName]['_Environment_']);
		}

		return $config;
	}

	/**
	 * @param mixed|null $default
	 * @return mixed
	 */
	private function safeRequire(string $path, $default = null) {
		if (!file_exists($path)) {
			return $default;
		}

		ob_start();
		$return = require($path) ?: $default;
		ob_end_clean();

		return $return;
	}
}
