<?php

declare(strict_types=1);

namespace Arrow\Object;

use Arrow\Interface\Middleware;

class RouteObject {
	/**
	 * @param class-string<Middleware>[] $middlewares
	 */
	public function __construct(
		public readonly string $method,
		public readonly string $route,
		public readonly RouteCallable $callable,
		public readonly array $middlewares,
	) {}
}