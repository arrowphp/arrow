<?php

declare(strict_types=1);

namespace Arrow\Object;

use Arrow\Formatter\ResponseFormatter;
use GuzzleHttp\Psr7\Response;

class Action {

	/**
	 * @param mixed $data
	 * @param mixed $error
	 * @param array<string, string[]> $headers
	 */
	public function __construct(
		public int $status = 200,
		public mixed $data = null,
		public mixed $error = null,
		public array $headers = [],
		protected ?ResponseFormatter $formatter = null,
	) {}

	public function setFormatter(ResponseFormatter $formatter): void {
		$this->formatter = $formatter;
	}

	public function toResponse(ResponseFormatter $fallbackFormatter): Response {
		$formatter = $this->formatter;
		$formatter ??= $fallbackFormatter;

		return $formatter->format($this);
	}
}
