<?php

declare(strict_types=1);

namespace Arrow\Object;

class RouteEndpoint {
	public function __construct(
		public readonly string $regex,
		public readonly RouteCallable $callable,
	) {}
}
