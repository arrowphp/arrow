<?php

declare(strict_types=1);

namespace Arrow\Formatter;

class ResponseJsonFormatter extends ResponseFormatter {

	protected function formatData(mixed $data): mixed {
		if ($data === null || $data === '') {
			return '';
		}

		if (is_scalar($data) || is_array($data)) {
			return json_encode($data);
		}

		return $data;
	}

}
