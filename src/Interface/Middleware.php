<?php

declare(strict_types=1);

namespace Arrow\Interface;

use Arrow\Object\Action;
use GuzzleHttp\Psr7\Request;

interface Middleware {

	// TODO is this right?
	public function run(callable $next, Request $request): Action;

}
