<?php

declare(strict_types=1);

namespace Arrow\Interface;

/**
 * @template T
 * @template N
 */
interface Formatter {

	/**
	 * @param T $data
	 * @return T
	 */
	public function format(mixed $data): mixed;

}
