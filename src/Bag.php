<?php

declare(strict_types=1);

namespace Arrow;

class Bag {

	/** @var array<mixed, mixed> */
	protected array $data = [];

	/**
	 * @param array<mixed, mixed> $data
	 */
	public function __construct(array $data = []) {
		$this->data = $data;
	}

	/**
	 * @param array<mixed, mixed> $data
	 */
	public function init(array $data): void {
		$this->data = array_replace_recursive($data, $this->data);
	}

	/**
	 * @param string|string[] $path
	 */
	public function has(string|array $path): bool {
		try {
			$this->get($path);
		} catch (\Exception $exception) {
			return false;
		}

		return true;
	}

	/**
	 * @param string|string[]|null $path
	 * @return mixed
	 */
	public function get(string|array $path = null): mixed {
		return $this->find($path);
	}

	/**
	 * @param string|string[]|null $path
	 * @return mixed
	 */
	public function tryGet(string|array $path = null) {
		try {
			return $this->get($path);
		} catch (\Exception $exception) {
			throw $exception;
		}
	}

	/**
	 * @param string|string[] $path
	 * @param mixed $value
	 */
	public function add(string|array $path, mixed $value): bool {
		$data = &$this->find($path);

		if (!is_array($data)) {
			throw (new Exception('Can not add value; invalid path, not an array.'))
				->extendContext(['Path' => $path]);
		}

		$data[] = $value;

		return true;
	}

	/**
	 * @param string|string[] $path
	 * @param mixed $value
	 */
	public function tryAdd(string|array $path, mixed $value): bool {
		try {
			return $this->add($path, $value);
		} catch (\Exception $exception) {
			return false;
		}
	}

	/**
	 * @param string|string[] $path
	 * @param mixed $value
	 */
	public function set(string|array $path, mixed $value): bool {
		$data = &$this->data;

		$parts = is_string($path)
			? explode('.', $path)
			: $path;

		// Remove the last key to use for setting the value
		$lastKey = array_pop($parts);

		while (($key = array_shift($parts))) {
			if (!array_key_exists($key, $data)) {
				$data[$key] = [];
			}

			$data = &$data[$key];

			if (!is_array($data)) {
				throw (new Exception('Can not set value; invalid path, not an array.'))
					->extendContext(['Path' => $path, 'Key' => $key]);
			}
		}

		$data[$lastKey] = $value;

		return true;
	}

	/**
	 * @param string|string[]|null $path
	 * @return mixed
	 */
	private function &find(string|array $path = null): mixed {
		$data = &$this->data;

		if ($path === null) {
			return $data;
		}

		$parts = is_string($path)
			? explode('.', $path)
			: $path;

		if (count($parts) === 0) {
			return $data;
		}

		while (($key = array_shift($parts))) {
			if (array_key_exists($key, $data)) {
				$data = &$data[$key];
			} else {
				throw (new Exception('Can not find value; invalid path.'))
					->extendContext(['Path' => $path, 'Key' => $key, 'Data' => $data]);
			}
		}

		return $data;
	}

	/**
	 * @return array<mixed, mixed>
	 */
	public function toArray(): array {
		return $this->data;
	}
}
