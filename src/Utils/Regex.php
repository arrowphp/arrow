<?php

namespace Arrow\Utils;

class Regex {

	private function __construct() {}

	/**
	 * @template TFlags is int
	 * @param TFlags $flags
	 * @return array<array<int, int|string|null>|string|null>
	 */
	public static function match(string $regex, string $subject, int $flags = 0, int $offset = 0): array|null {
		$match = preg_match($regex, $subject, $matches, $flags, $offset);

		assert(preg_last_error() === PREG_NO_ERROR, new RegexLastErrorException($regex));
		assert($match !== false, new RegexException($regex));

		if ($match === 0) {
			return null;
		}

		return $matches;
	}

	/**
	 * @return array<int|string, string[]>|array<array<int, int|string|null>|string|null>
	 */
	public static function match_all(string $regex, string $subject, int $flags = 0, int $offset = 0): array {
		$match = preg_match_all($regex, $subject, $matches, $flags, $offset);

		assert(preg_last_error() === PREG_NO_ERROR, new RegexLastErrorException($regex));
		assert($match !== false, new RegexException($regex));

		if ($match === 0) {
			return [];
		}

		return $matches;
	}

	/**
	 * @template T as string|string[]
	 * @param string|string[] $regex
	 * @param string|string[] $replacements
	 * @param T $subject
	 * @return (T is string ? string : array<int, string>)
	 */
	public static function replace(string|array $regex, string|array $replacements, string|array $subject, int $limit = 0, int &$count = 0): string|array {
		$result = preg_replace($regex, $replacements, $subject, $limit, $count);

		assert(preg_last_error() === PREG_NO_ERROR, new RegexLastErrorException($regex));
		assert($result !== null, new RegexException($regex));

		return $result;
	}

}

class RegexException extends \Arrow\Exception {
	/**
	 * @param string|string[] $regex
	 */
	public function __construct(string|array $regex, string $message = 'Error', int $code = null) {
		parent::__construct(
			message: "Regex Exception: $message",
			context: [
				'regex' => $regex,
				'code' => $code,
				'message' => $message,
			],
		);
	}
}

class RegexStringException extends RegexException {
	/**
	 * @param string|string[] $regex
	 */
	public function __construct(string|array $regex) {
		parent::__construct(
			regex: $regex,
			message: "Regex expected a string, received array",
		);
	}
}

class RegexLastErrorException extends \Arrow\Exception {
	/**
	 * @param string|string[] $regex
	 */
	public function __construct(string|array $regex) {
		parent::__construct(
			message: preg_last_error_msg(),
			context: [
				'regex' => $regex,
				'errorCode' => preg_last_error(),
				'errorMsg' => preg_last_error_msg(),
			],
		);
	}
}
