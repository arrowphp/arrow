<?php

declare(strict_types=1);

namespace Arrow\Trait;

use Arrow\Object\Action as ObjectAction;

trait ActionApi {
	use Action {
		Action::action as parentAction;
	}

	protected function actionData(int $status, mixed $data = null, mixed $error = null): mixed {
		$body = ['status' => $status];

		if ($error !== null) {
			$body['error'] = $error;
		} else {
			$body['data'] = $data;
		}

		return $body;
	}

	protected function action(
		int $status,
		mixed $data = null,
		mixed $error = null,
		array $headers = [],
	): ObjectAction {
		return $this->parentAction(
			200,
			$this->actionData($status, $data, $error),
			null,
			$headers
		);
	}

	protected function ok(
		mixed $data,
	): ObjectAction {
		return $this->action(200, $data);
	}

	protected function error(string $reason = null, int $status = 500) {
		return $this->action($status, null, $reason);
	}

	protected function notFound(string $reason = null) {
		return $this->error($reason, 404);
	}

}
